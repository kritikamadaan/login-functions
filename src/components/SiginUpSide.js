import React,{useState,useEffect} from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import Paper from '@material-ui/core/Paper';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import axios from "axios";

function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'Copyright © '}
      <Link color="inherit" href="https://material-ui.com/">
        Your Website
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

const useStyles = makeStyles((theme) => ({
  root: {
    height: '100vh',
  },
  image: {
    backgroundImage: 'url(https://source.unsplash.com/random)',
    backgroundRepeat: 'no-repeat',
    backgroundColor:
      theme.palette.type === 'light' ? theme.palette.grey[50] : theme.palette.grey[900],
    backgroundSize: 'cover',
    backgroundPosition: 'center',
  },
  paper: {
    margin: theme.spacing(8, 4),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

export default function SignUpSide() {
  const classes = useStyles();
  const [countryCode, setCountryCode] = useState("");
  const [phoneNumber, setPhoneNumber] = useState("");
  const [message, setMessage] = useState("");
  const [otp, setOtp] = useState("");
  const [userOtp, setUserOtp] = useState("");
  const [submitResponse, setSubmitResponse] = useState(false);

  let isMounted = true; // note this flag denote mount status
  const token = localStorage.getItem("token");

  // for second req
  const [errors, setErrors] = useState({
    email: "",
    password: "",
  });
  const [isValid, setIsValid] = useState(false);
  

  const validation = () => {
    const validEmailRegex = RegExp(
      /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i
    );
    let isValid = true;
    let errors = {};

    if (countryCode === "") {
      isValid = false;
      errors["countryCode"] = "Country Code is required.";
    }
    if (phoneNumber === "") {
      isValid = false;
      errors["phoneNumber"] = "Phone Number is required.";
    }
    if (userOtp === "") {
      isValid = false;
      errors["userOtp"] = " Otp is Required.";
    }
    if (userOtp.length < 6) {
      isValid = false;
      errors["userOtp"] = "Otp must be six in length.";
    }

    setIsValid(isValid);
    setErrors(errors);
  };

  const submitHandler = (e) => {
    e.preventDefault();
    validation();
    const phonedata = {
      country: countryCode,
      phone: phoneNumber,
    };
    axios
      .post("https://admin.pwip.co/api/login_snd_code/", phonedata)
      .then((response) => {
        if (response.status === 200) {
          setCountryCode(response.data.country);
          //setPhoneNumber(response.data.phone)
          setMessage(response.data.msg);
          setOtp(response.data.OTP);
          setSubmitResponse(true);
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const verificationCall = (e) => {
    e.preventDefault();
    validation();
    console.log("verification api", countryCode, phoneNumber, userOtp);
    const verifydata = {
      country: countryCode,
      phone: phoneNumber,
      code: userOtp,
    };
    console.log("verification data ", verifydata);
    axios
      .post("https://admin.pwip.co/api/login_verify_code/", verifydata)
      .then((response) => {
        console.log("response in  verification api*****", response);

        if (response.status === 200) {
          localStorage.setItem("token", response.data.token);
          setCountryCode(response.data.country);
          setPhoneNumber(response.data.phone);
          setMessage(response.data.msg);
          setOtp(response.data.OTP);
          window.location.href = "/dashboard/products";
        }
      })
      .catch((err) => {
        console.log("response is", err);
      });
  };

  useEffect(() => {
    if (token) {
      // window.location.href = "/dashboard/products";
    } else {
      return null;
    }
    return () => {
      isMounted = false;
    };
  }, []);



  return (
    <Grid container component="main" className={classes.root}>
      <CssBaseline />
      <Grid item xs={false} sm={4} md={7} className={classes.image} />
      <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
        <div className={classes.paper}>
          <Avatar className={classes.avatar}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            Sign Up
          </Typography>
          <form className={classes.form} noValidate>
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              id="email"
              label="Email Address"
              name="email"
              autoComplete="email"
              autoFocus
            />
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              name="password"
              label="Password"
              type="password"
              id="password"
              autoComplete="current-password"
            />
            <FormControlLabel
              control={<Checkbox value="remember" color="primary" />}
              label="Remember me"
            />
            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
            >
              Sign Up
            </Button>
            <Grid container>
              <Grid item xs>
                <Link href="#" variant="body2">
                  Forgot password?
                </Link>
              </Grid>
              <Grid item>
                <Link href="/" variant="body2">
                  {"Don't have an account? Sign Up"}
                </Link>
              </Grid>
            </Grid>
            <Box mt={5}>
              <Copyright />
            </Box>
          </form>
        </div>
      </Grid>
    </Grid>
  );
}
