import { SettingsInputSvideo } from '@material-ui/icons';
import React,{Fragment} from 'react';
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect,
} from "react-router-dom";
import SignUpSide from './components/SiginUpSide';
import SignInSide from './components/SigninSide';

const Routes = () => {
  return (
  <Fragment>
    <Switch>
    <Route exact path={["/","/signin","login"]} component={SignInSide} />
    <Route exact path={["/signup","register"]} component={SignUpSide} />
    </Switch>
  </Fragment>
  );
}
export default Routes;
